"""
Exercices.

Implémentez les méthodes ci-dessous.
Pour executer vos tests il vous faudra utiliser pytest
"""


def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float:
    """
    Exercice 1 :
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    if discount_rate < 0 or discount_rate > 1:
        raise ValueError
    else:
        tax = 0.206
        tax_factor = 1 + tax
        discount_factor = 1 - discount_rate
        ttc_cost = htc_cost * tax_factor * discount_factor
        return round(ttc_cost, 2)


def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """
    divisors_list = []

    for n in range(2, value, 1):
        while value % n == 0:
            divisors_list.append(n)
            value = value / n
    if not divisors_list:
        return "PREMIER"
    else:
        return set(divisors_list)
